﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Domain.Entities;
using TOFIProject_Web.Models.Documentations;
using TOFIProject_Web.Models.Manager;
using TOFIProject_Web.Models.Messages;
using TOFIProject_Web.Models.Projects;
using TOFIProject_Web.Services.Abstract;

namespace TOFIProject_Web.Controllers
{
    [Authorize(Roles = "Manager")]
    public class ManagerController : Controller
    {
        private ICampaignService _campaignService;
        private IProjectService _projectService;
        private IMessageService _messageService;
        private IUserService _userService;
        private IDocumentationService _docService;
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _signInManager;

        public ManagerController(ICampaignService campaignService,
                                IProjectService projectService,
                                IMessageService messageService,
                                IUserService userService,
                                IDocumentationService docService,
                            UserManager<AppUser> userManager,
                            SignInManager<AppUser> signInManager)
        {
            _campaignService = campaignService;
            _projectService = projectService;
            _messageService = messageService;
            _userService = userService;
            _userManager = userManager;
            _signInManager = signInManager;
            _docService = docService;
        }

        public IActionResult Index()
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var users = _userService.GetUserByCampaignId(user.CampaignId.Value);
            var campaign = _campaignService.GetById(user.CampaignId.Value);
            var projectlist = _projectService.GetByCampaignId(user.CampaignId.Value);
            var model = new IndexViewModel { CampaignName = campaign.Result.Name, Projects = projectlist.Result, Users = users };
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> CreateProject()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);

            return View(new ProjectModel {});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateProject(ProjectModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                model.Campaign = user.Campaign;
                model.CampaignId = user.CampaignId.Value;
                _projectService.AddOrUpdate(model);
                return RedirectToAction("Index", "Manager");                           
            }
            return View();
        }

        public async Task<IActionResult> Project(int id)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var project = _projectService.GetById(id).Result;
            if (project.CampaignId != user.CampaignId)
                return BadRequest();

            var messages = _messageService.GetByProjectId(project.Id).Result;
            messages = messages.OrderByDescending(x => x.Id).Take(20);
            var docs = _docService.GetByProjectId(project.Id).Result;
            //todo automapper or rewrite
            var smallDocs = docs.Select<DocumentationModel, SmallDocModel>(x => new SmallDocModel { Id = x.Id, Name = x.Name });

            var model = new ProjectViewModel { Project = project, Docs = smallDocs, Messages = messages };
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> AddMessage( int projectId)
        {
            return View(new MessageModel { ProjectId = projectId});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddMessage(MessageModel model)
        {
            if (ModelState.IsValid)
            {

                var result = _projectService.GetById(model.ProjectId).Result;
                if (result == null)
                {
                    ModelState.AddModelError("", "Invalid project Id");
                    return View();
                }

                var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
                var campaign = _campaignService.GetById(result.CampaignId).Result;

                if (user.CampaignId.Value != campaign.Id)
                {
                    ModelState.AddModelError("", "Not allowed");
                    return View();
                }
                model.AppUser = user;

                _messageService.AddOrUpdate(model);

                return RedirectToAction("Project", "Manager", new { id = model.ProjectId });

            }
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetFile(int id)
        {
            var result = await _docService.GetById(id);
            if (result == null)
                return BadRequest();

            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var project = await _projectService.GetById(result.ProjectId);
            if (user.CampaignId.Value != project.CampaignId)
                return BadRequest();

            return File(result.Data, result.ContentType, result.Name);
        }
    }
}