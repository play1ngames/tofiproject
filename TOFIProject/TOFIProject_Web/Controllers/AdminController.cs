﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Domain.Entities;
using TOFIProject_Web.Models.Admin;
using TOFIProject_Web.Models.Documentations;
using TOFIProject_Web.Models.Messages;
using TOFIProject_Web.Services.Abstract;

namespace TOFIProject_Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        UserManager<AppUser> _userManager;
        IUserService _userService;
        IProjectService _projectService;
        IMessageService _messageService;
        IDocumentationService _docService;

        public AdminController(UserManager<AppUser> userManager,
            IUserService userService,
            IProjectService projectService,
                    IMessageService messageService,
        IDocumentationService docService)
        {
            _userManager = userManager;
            _userService = userService;
            _projectService = projectService;
            _messageService = messageService;
            _docService = docService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var users = _userService.GetUsersByRole("User");
            var projects = await _projectService.GetAsync(); 

            var model = new IndexViewModel { Users = users, Projects = projects };
            return View(model);
        }

        public async Task<IActionResult> Project(int id)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var project = _projectService.GetById(id).Result;
            var messages = _messageService.GetByProjectId(project.Id).Result;
            messages = messages.OrderByDescending(x => x.Id).Take(20);
            var docs = _docService.GetByProjectId(project.Id).Result;
            //todo automapper or rewrite
            var smallDocs = docs.Select<DocumentationModel, SmallDocModel>(x => new SmallDocModel { Id = x.Id, Name = x.Name });

            var model = new ProjectViewModel { Project = project, Docs = smallDocs, Messages = messages };
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> AddMessage(int projectId)
        {
            return View(new MessageModel { ProjectId = projectId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddMessage(MessageModel model)
        {
            if (ModelState.IsValid)
            {

                var result = _projectService.GetById(model.ProjectId).Result;
                if (result == null)
                {
                    ModelState.AddModelError("", "Invalid project Id");
                    return View();
                }

                var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
                model.AppUser = user;

                _messageService.AddOrUpdate(model);

                return RedirectToAction("Project", "Admin", new { id = model.ProjectId });

            }
            return View();
        }


        [HttpGet]
        public async Task<IActionResult> AddFile(int projectId)
        {
            return View(new FileModel { ProjectId = projectId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddFile(FileModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.File == null)
                {
                    ModelState.AddModelError("", "Invalid file");
                    return View(model);
                }

                var result = await _projectService.GetById(model.ProjectId);
                if (result == null)
                {
                    ModelState.AddModelError("", "Invalid project Id");
                    return View(model);
                }

                byte[] imageData = null;
                // считываем переданный файл в массив байтов
                using (var binaryReader = new BinaryReader(model.File.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)model.File.Length);
                }

                _docService.AddOrUpdate(new DocumentationModel
                {
                    ContentType = model.File.ContentType,
                    Name = model.File.FileName,
                    Project = result,
                    ProjectId = model.ProjectId,
                    Data = imageData
                });

                return RedirectToAction("Project", "Admin", new { id = model.ProjectId });

            }
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetFile(int id)
        {
            var result = await _docService.GetById(id);
            if (result == null)
                return BadRequest();

            return File(result.Data, result.ContentType, result.Name);
        }


    }
}