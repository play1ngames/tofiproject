﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TOFIProject_Domain.Entities;
using TOFIProject_Web.Models.Campaigns;
using TOFIProject_Web.Models.User;
using TOFIProject_Web.Services.Abstract;

namespace TOFIProject_Web.Controllers
{
    [Authorize(Roles = "User")]
    public class UserController : Controller
    {
        private ICampaignService _campaignService;
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _signInManager;

        public UserController(ICampaignService campaignService,
                            UserManager<AppUser> userManager,
                            SignInManager<AppUser> signInManager)
        {
            _campaignService = campaignService;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Profile()
        {
            var model = _campaignService.GetAsync().Result;
            return View(new AllCampaignsViewModel { Campaigns = model });
        }

        [HttpGet]
        public IActionResult CreateCampaign()
        {
            return View(new CreateCampaignViewModel { });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCampaign(CreateCampaignViewModel model)
        {
            if (ModelState.IsValid)
            {
                
                var result =  _campaignService.GetByName(model.Name);
                if(result != null)
                {
                    ModelState.AddModelError("", "Invalid Name");
                    return View();
                }

                var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
                _campaignService.AddOrUpdate(new CampaignModel { Name = model.Name, Users = new List<AppUser> { user } });

                var result2 = _userManager.AddToRoleAsync(user, "Manager").Result;
                var result3 = _userManager.RemoveFromRoleAsync(user, "User").Result;
                await _signInManager.SignOutAsync();
                return RedirectToAction("Login", "Account");
                
            }
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ChooseCampaign(int id)
        {
            var result = _campaignService.GetById(id).Result;
            if (result == null)
            {
                return BadRequest();
            }

            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            user.CampaignId = result.Id;
            user.Campaign = result;

            var result2 = _userManager.AddToRoleAsync(user, "Manager").Result;
            var result3 = _userManager.RemoveFromRoleAsync(user, "User").Result;
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        [Authorize(Roles ="User,Manager,Admin")]
        public async Task<IActionResult> GetNameById(string id)
        {
            var user = _userManager.FindByIdAsync(id).Result;
            if (user == null)
                return new NotFoundResult();
            return Content(user.UserName);
        }

    }
}