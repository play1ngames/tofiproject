﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Domain.Entities;
using TOFIProject_Web.Models.Documentations;
using TOFIProject_Web.Services.Abstract;

namespace TOFIProject_Web.Services.Concrete
{
    public class DocumentationService: IDocumentationService
    {
        private readonly IBaseService<Documentation> _service;
        private readonly IMapper _mapper;
        //private readonly IPositionService _posService;

        public DocumentationService(IBaseService<Documentation> service, IMapper mapper)//, IPositionService posService)
        {
            _service = service;
            _mapper = mapper;
            //_posService = posService;
        }

        public void AddOrUpdate(DocumentationModel entry)
        {
            //todo validation
            //if (_posService.GetById(entry.PositionId).Result == null)
            //    throw new ArgumentException(String.Format("{0} not found", entry.PositionId));
            _service.AddOrUpdate(_mapper.Map<DocumentationModel, Documentation>(entry));
        }

        public async Task<IEnumerable<DocumentationModel>> GetAsync()
        {
            var result = await _service.GetAsync();
            return result.Select(p => _mapper.Map<Documentation, DocumentationModel>(p));
        }

        public async Task<DocumentationModel> GetById(int id)
        {
            var result = await _service.GetById(id);
            return _mapper.Map<Documentation, DocumentationModel>(result);
        }

        public async Task<IEnumerable<DocumentationModel>> GetByProjectId(int projectId)
        {
            var result = await GetAsync();
            return result.Where(p => p.ProjectId == projectId);
        }

        public void Remove(int id)
        {
            _service.Remove(id);
        }

        //public IEnumerable<CampaignModel> Where(EmployeeRequestModel request)
        //{
        //    var result = _service.Where(_mapper.Map<EmployeeRequestModel, FilterOrderModel>(request));
        //    return _mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeModel>>(result);
        //}

    }
}
