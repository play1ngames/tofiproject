﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Domain.Entities;
using TOFIProject_Web.Models.Avatars;
using TOFIProject_Web.Services.Abstract;

namespace TOFIProject_Web.Services.Concrete
{
    //class AvatarService
    //{
    //    private readonly IBaseService<Avatar> _service;
    //    private readonly IMapper _mapper;
    //    //private readonly IPositionService _posService;

    //    public AvatarService(IBaseService<Avatar> service, IMapper mapper)//, IPositionService posService)
    //    {
    //        _service = service;
    //        _mapper = mapper;
    //        //_posService = posService;
    //    }

    //    public void AddOrUpdate(AvatarModel entry)
    //    {
    //        //todo validation
    //        //if (_posService.GetById(entry.PositionId).Result == null)
    //        //    throw new ArgumentException(String.Format("{0} not found", entry.PositionId));
    //        _service.AddOrUpdate(_mapper.Map<AvatarModel, Avatar>(entry));
    //    }

    //    public async Task<IEnumerable<AvatarModel>> GetAsync()
    //    {
    //        var result = await _service.GetAsync();
    //        return result.Select(p => _mapper.Map<Avatar, AvatarModel>(p));
    //    }

    //    public async Task<AvatarModel> GetById(int id)
    //    {
    //        var result = await _service.GetById(id);
    //        return _mapper.Map<Avatar, AvatarModel>(result);
    //    }

    //    public void Remove(int id)
    //    {
    //        _service.Remove(id);
    //    }

    //    //public IEnumerable<CampaignModel> Where(EmployeeRequestModel request)
    //    //{
    //    //    var result = _service.Where(_mapper.Map<EmployeeRequestModel, FilterOrderModel>(request));
    //    //    return _mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeModel>>(result);
    //    //}
    //}
}
