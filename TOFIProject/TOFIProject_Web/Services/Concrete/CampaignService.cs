﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Domain.Entities;
using TOFIProject_Web.Models.Campaigns;
using TOFIProject_Web.Services.Abstract;

namespace TOFIProject_Web.Services.Concrete
{
    public class CampaignService : ICampaignService
    {
        private readonly IBaseService<Campaign> _service;
        private readonly IMapper _mapper;
        //private readonly IPositionService _posService;

        public CampaignService(IBaseService<Campaign> service, IMapper mapper)//, IPositionService posService)
        {
            _service = service;
            _mapper = mapper;
            //_posService = posService;
        }

        public void AddOrUpdate(CampaignModel entry)
        {
            //todo validation
            //if (_posService.GetById(entry.PositionId).Result == null)
            //    throw new ArgumentException(String.Format("{0} not found", entry.PositionId));
            _service.AddOrUpdate(_mapper.Map<CampaignModel, Campaign>(entry));
        }

        public async Task<IEnumerable<CampaignModel>> GetAsync()
        {
            var result = await _service.GetAsync();
            return result.Select(p => _mapper.Map<Campaign, CampaignModel>(p));
        }

        public async Task<CampaignModel> GetById(int id)
        {
            var result = await _service.GetById(id);
            return _mapper.Map<Campaign, CampaignModel>(result);
        }

        public CampaignModel GetByName(string Name)
        {
            var result =  _service.GetAsync().Result.FirstOrDefault(x=>x.Name.Equals(Name));
            return _mapper.Map<Campaign, CampaignModel>(result);
        }

        public void Remove(int id)
        {
            _service.Remove(id);
        }

        //public IEnumerable<CampaignModel> Where(EmployeeRequestModel request)
        //{
        //    var result = _service.Where(_mapper.Map<EmployeeRequestModel, FilterOrderModel>(request));
        //    return _mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeModel>>(result);
        //}

    }
}
