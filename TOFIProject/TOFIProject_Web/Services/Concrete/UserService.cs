﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using TOFIProject_Domain.Entities;
using TOFIProject_Domain.Repositories;
using TOFIProject_Web.Models.Users;
using TOFIProject_Web.Services.Abstract;

namespace TOFIProject_Web.Services.Concrete
{
    public class UserService : IUserService
    {
        private IUserRepository _repository;
        private UserManager<AppUser> _userManager;
        private readonly IMapper _mapper;

        public UserService(IUserRepository repository, IMapper mapper, UserManager<AppUser> userManager)
        {
            _repository = repository;
            _mapper = mapper;
            _userManager = userManager;
        }

        public UserModel ValidateUser(string email, string password)
        {
            var appUser = _repository.GetByEmail(email);
            var res = _repository.GetUserManager().CheckPasswordAsync(appUser, password);
            if (res.Result)
                return _mapper.Map<AppUser, UserModel>(appUser);
            else return null;
        }

        public IEnumerable<UserModel> GetUserByCampaignId(int id)
        {
            var result = _repository.Get().AsEnumerable().Where(x => x.CampaignId == id);
            return _mapper.Map<IEnumerable<AppUser>, IEnumerable<UserModel>>(result);
        }

        public IEnumerable<UserModel> GetUsersByRole(string role)
        {
            var users =  _userManager.GetUsersInRoleAsync(role);
            return _mapper.Map<IEnumerable<AppUser>, IEnumerable<UserModel>>(users.Result);
        }

        //public string GetToken(ClaimsIdentity identity)
        //{
        //    var now = DateTime.UtcNow;
        //    // create JWT-token
        //    var jwt = new JwtSecurityToken(
        //            issuer: AuthOptions.ISSUER,
        //            audience: AuthOptions.AUDIENCE,
        //            notBefore: now,
        //            claims: identity.Claims,
        //            expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
        //            signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
        //    return new JwtSecurityTokenHandler().WriteToken(jwt);
        //}

        //public ClaimsIdentity GetIdentity(string email, string password)
        //{
        //    var person = ValidateUser(email, password);
        //    if (person == null)
        //        return null;

        //    var claims = new List<Claim>
        //    {
        //        new Claim(ClaimsIdentity.DefaultNameClaimType, person.Email),
        //    };
        //    ClaimsIdentity claimsIdentity = new ClaimsIdentity(
        //        claims,
        //        "Token",
        //        ClaimsIdentity.DefaultNameClaimType,
        //        ClaimsIdentity.DefaultRoleClaimType
        //        );
        //    return claimsIdentity;
        //}
    }
}
