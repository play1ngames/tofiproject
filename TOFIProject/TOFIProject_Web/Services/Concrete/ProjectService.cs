﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Domain.Entities;
using TOFIProject_Web.Models.Projects;
using TOFIProject_Web.Services.Abstract;

namespace TOFIProject_Web.Services.Concrete
{
    public class ProjectService: IProjectService
    {
        private readonly IBaseService<Project> _service;
        private readonly IMapper _mapper;
        //private readonly IPositionService _posService;

        public ProjectService(IBaseService<Project> service, IMapper mapper)//, IPositionService posService)
        {
            _service = service;
            _mapper = mapper;
            //_posService = posService;
        }

        public void AddOrUpdate(ProjectModel entry)
        {
            //todo validation
            //if (_posService.GetById(entry.PositionId).Result == null)
            //    throw new ArgumentException(String.Format("{0} not found", entry.PositionId));
            _service.AddOrUpdate(_mapper.Map<ProjectModel, Project>(entry));
        }

        public async Task<IEnumerable<ProjectModel>> GetAsync()
        {
            var result = await _service.GetAsync();
            return result.Select(p => _mapper.Map<Project, ProjectModel>(p));
        }

        public async Task<IEnumerable<ProjectModel>> GetByCampaignId(int campaignId)
        {
            var result = await _service.GetAsync();
            result = result.Where(x => x.CampaignId == campaignId);
            return _mapper.Map<IEnumerable<Project>, IEnumerable<ProjectModel>>(result);
        }

        public async Task<ProjectModel> GetById(int id)
        {
            var result = await _service.GetById(id);
            return _mapper.Map<Project, ProjectModel>(result);
        }

        public void Remove(int id)
        {
            _service.Remove(id);
        }

        //public IEnumerable<CampaignModel> Where(EmployeeRequestModel request)
        //{
        //    var result = _service.Where(_mapper.Map<EmployeeRequestModel, FilterOrderModel>(request));
        //    return _mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeModel>>(result);
        //}

    }
}
