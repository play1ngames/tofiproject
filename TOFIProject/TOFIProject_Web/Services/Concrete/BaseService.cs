﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TOFIProject_Domain.Entities;
using TOFIProject_Domain.Repositories;
using TOFIProject_Web.Services.Abstract;

namespace TOFIProject_Web.Services.Concrete
{
    public class BaseService<T> : IBaseService<T> where T : BaseEntity
    {
        private readonly IBaseRepository<T> _repository;

        public BaseService(IBaseRepository<T> repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<T>> GetAsync()
        {
            return await _repository.GetAll();
        }

        public async Task<T> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        //public IEnumerable<T> Where(FilterOrderModel request)
        //{
        //    var result = GetAsync().Result;

        //    if (request == null)
        //        return result;

        //    if (request.Filters != null)
        //    {
        //        foreach (var filter in request.Filters)
        //        {
        //            try
        //            {
        //                result = result.Where(o => filter.FieldValues.Contains(o.GetType().GetProperty(filter.Field).GetValue(o, null).ToString())).ToList();
        //            }
        //            catch (Exception e)
        //            {
        //                throw new ArgumentException(String.Format("Bad filter: {0}", filter.Field));
        //            }
        //        }
        //    }

        //    if (request.OrderBy != null)
        //    {
        //        try
        //        {
        //            if (request.OrderBy.IsDesc)
        //                result = result.OrderByDescending(o => o.GetType()
        //                                              .GetProperty(request.OrderBy.Field)
        //                                              .GetValue(o, null)).ToList();
        //            else
        //                result = result.OrderBy(o => o.GetType()
        //                          .GetProperty(request.OrderBy.Field)
        //                          .GetValue(o, null)).ToList();
        //        }
        //        catch (Exception e)
        //        {
        //            throw new ArgumentException(String.Format("Bad orderby: {0}", request.OrderBy.Field));
        //        }
        //    }

        //    return result;
        //}

        public void AddOrUpdate(T entry)
        {
            var targetRecord = _repository.GetById(entry.Id).Result;
            var exists = targetRecord != null;

            if (exists)
                _repository.Update(entry);
            else if (entry.Id == 0)
                _repository.Insert(entry);
            else
                throw new ArgumentException("id != 0 || id is not in db");
        }

        public void Remove(int id)
        {
            var label = _repository.GetById(id).Result;
            if (label == null)
                throw new ArgumentException("id not found");
            _repository.Delete(label);
        }
    }
}
