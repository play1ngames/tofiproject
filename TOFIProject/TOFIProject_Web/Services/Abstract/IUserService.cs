﻿using System.Collections.Generic;
using TOFIProject_Web.Models.Users;

namespace TOFIProject_Web.Services.Abstract
{
    public interface IUserService
    {
        UserModel ValidateUser(string email, string password);
        IEnumerable<UserModel> GetUserByCampaignId(int id);
        IEnumerable<UserModel> GetUsersByRole(string role);
        //  ClaimsIdentity GetIdentity(string email, string password);

        //string GetToken(ClaimsIdentity identity);
    }
}
