﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Domain.Entities;

namespace TOFIProject_Web.Services.Abstract
{
    public interface IBaseService<T> where T : BaseEntity
    {

        Task<IEnumerable<T>> GetAsync();

        Task<T> GetById(int id);

        //IEnumerable<T> Where(FilterOrderModel request);

        void AddOrUpdate(T entry);

        void Remove(int id);
    }
}
