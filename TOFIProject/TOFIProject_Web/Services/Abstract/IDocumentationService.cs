﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Web.Models.Documentations;

namespace TOFIProject_Web.Services.Abstract
{
    public interface IDocumentationService
    {
        void AddOrUpdate(DocumentationModel entry);
        Task<IEnumerable<DocumentationModel>> GetAsync();
        Task<DocumentationModel> GetById(int id);
        Task<IEnumerable<DocumentationModel>> GetByProjectId(int projectId);
        void Remove(int id);
    }
}
