﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Web.Models.Messages;

namespace TOFIProject_Web.Services.Abstract
{
    public interface IMessageService
    {
        void AddOrUpdate(MessageModel entry);
        Task<IEnumerable<MessageModel>> GetAsync();
        Task<MessageModel> GetById(int id);
        Task<IEnumerable<MessageModel>> GetByProjectId(int projectId);
        void Remove(int id);
    }
}
