﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TOFIProject_Web.Models.Campaigns;

namespace TOFIProject_Web.Services.Abstract
{
    public interface ICampaignService
    {
        void AddOrUpdate(CampaignModel entry);
        Task<IEnumerable<CampaignModel>> GetAsync();
        Task<CampaignModel> GetById(int id);
        CampaignModel GetByName(string Name);
        void Remove(int id);
        //IEnumerable<EmployeeModel> Where(RequestModel request);
    }
}
