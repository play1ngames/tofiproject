﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Web.Models.Projects;

namespace TOFIProject_Web.Services.Abstract
{
    public interface IProjectService
    {
        void AddOrUpdate(ProjectModel entry);
        Task<IEnumerable<ProjectModel>> GetAsync();
        Task<ProjectModel> GetById(int id);
        Task<IEnumerable<ProjectModel>> GetByCampaignId(int campaignId);
        void Remove(int id);
    }
}
