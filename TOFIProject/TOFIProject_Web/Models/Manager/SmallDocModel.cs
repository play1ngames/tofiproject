﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOFIProject_Web.Models.Manager
{
    public class SmallDocModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
