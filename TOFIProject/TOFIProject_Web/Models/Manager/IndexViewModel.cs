﻿using System.Collections.Generic;
using TOFIProject_Web.Models.Projects;
using TOFIProject_Web.Models.Users;

namespace TOFIProject_Web.Models.Manager
{
    public class IndexViewModel
    {
        public string CampaignName { get; set; }
        public IEnumerable<UserModel> Users { get; set; }
        public IEnumerable<ProjectModel> Projects { get; set; } 
    }
}
