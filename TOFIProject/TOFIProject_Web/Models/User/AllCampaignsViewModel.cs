﻿using System.Collections.Generic;
using TOFIProject_Web.Models.Campaigns;

namespace TOFIProject_Web.Models.User
{
    public class AllCampaignsViewModel
    {
        public IEnumerable<CampaignModel> Campaigns { get; set; }
    }
}
