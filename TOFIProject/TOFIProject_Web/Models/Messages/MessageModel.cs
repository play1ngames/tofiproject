﻿using Microsoft.AspNetCore.Mvc;
using TOFIProject_Domain.Entities;

namespace TOFIProject_Web.Models.Messages
{
    public class MessageModel : Message
    {
        public new int ProjectId { get; set; }
    }
}
