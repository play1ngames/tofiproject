﻿using System.Collections.Generic;
using TOFIProject_Web.Models.Projects;
using TOFIProject_Web.Models.Users;

namespace TOFIProject_Web.Models.Admin
{
    public class IndexViewModel
    {
        public IEnumerable<UserModel> Users { get; set; }
        public IEnumerable<ProjectModel> Projects { get; set; }
    }
}
