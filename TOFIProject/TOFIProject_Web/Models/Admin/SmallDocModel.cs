﻿namespace TOFIProject_Web.Models.Admin
{
    public class SmallDocModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
