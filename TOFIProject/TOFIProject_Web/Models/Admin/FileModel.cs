﻿using Microsoft.AspNetCore.Http;

namespace TOFIProject_Web.Models.Admin
{
    public class FileModel
    {
        public int ProjectId { get; set; }
        public IFormFile File { get; set; }
    }
}
