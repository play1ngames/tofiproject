﻿using System.Collections.Generic;
using TOFIProject_Web.Models.Messages;
using TOFIProject_Web.Models.Projects;

namespace TOFIProject_Web.Models.Admin
{
    public class ProjectViewModel
    {
        public ProjectModel Project { get; set; }
        public IEnumerable<SmallDocModel> Docs { get; set; }
        public IEnumerable<MessageModel> Messages { get; set; } //last 20 messages order by date from laggest 

        //todo rewrite this models/controllers and views
    }
}
