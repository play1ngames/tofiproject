﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Domain.Contexts;
using TOFIProject_Domain.Entities;

namespace TOFIProject_Web
{
    public static class Extensions
    {

        public static IWebHost Migrate(this IWebHost webhost)
        {
            using (var serviceScope = webhost.Services.GetService<IServiceScopeFactory>().CreateScope())
            {
                using (Context db = serviceScope.ServiceProvider.GetService<Context>())
                {
                    // This will [try to] create database
                    // and apply all necessary migrations
                    db.Database.Migrate();


                    // then you can check for existing data and modify something
                    var admin = db.Users.Where(x => x.Email == "admin@admin.admin").FirstOrDefault();
                    if (admin == null)
                    {
                        admin = new AppUser { Email = "admin@admin.admin", UserName = "admin" };

                        var adminPassword = "Admin_123";
                        var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
                        var res1 = userManager.CreateAsync(admin, adminPassword).Result;
                        var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                        var newRole1 = new IdentityRole { Name = "User" };
                        var newRole2 = new IdentityRole { Name = "Manager" };
                        var newRole3 = new IdentityRole { Name = "Admin" };
                        
                        var res2 = roleManager.CreateAsync(newRole1).Result;
                        var res3 = roleManager.CreateAsync(newRole2).Result;
                        var res4 = roleManager.CreateAsync(newRole3).Result;

                        var adminUser = userManager.FindByEmailAsync("admin@admin.admin");
                        var res5 = userManager.AddToRoleAsync(adminUser.Result, "Admin").Result;
                        if(res5.Succeeded)
                            db.SaveChanges();

                    }
                }

            }
            return webhost;
        }
    }
}
