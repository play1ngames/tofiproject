﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TOFIProject_Domain.Contexts;
using TOFIProject_Domain.Entities;
using TOFIProject_Domain.Repositories;
using TOFIProject_Web.Services.Abstract;
using TOFIProject_Web.Services.Concrete;

namespace TOFIProject_Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<Context>(options => options.UseSqlServer(connection, b=>b.MigrationsAssembly("TOFIProject_Web")));

            // установка конфигурации подключения
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)

                .AddCookie(options => //CookieAuthenticationOptions
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                });

            services.AddIdentity <AppUser,IdentityRole> (options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
                options.Password.RequiredUniqueChars = 2;
                // Signin settings
                options.SignIn.RequireConfirmedEmail = false;
                options.SignIn.RequireConfirmedPhoneNumber = false;
                // User settings
                options.User.RequireUniqueEmail = true;
            })
                                            .AddEntityFrameworkStores<Context>()
                                            .AddDefaultTokenProviders();

            services.AddAutoMapper(typeof(Startup));

            #region DependencyInjection
            services.AddTransient<IBaseRepository<BaseEntity>, BaseRepository<BaseEntity>>();
            services.AddTransient<IBaseRepository<BaseFile>, BaseRepository<BaseFile>>();
            services.AddTransient<IBaseRepository<Campaign>, BaseRepository<Campaign>>();
            services.AddTransient<IBaseRepository<Documentation>, BaseRepository<Documentation>>();
            services.AddTransient<IBaseRepository<Message>, BaseRepository<Message>>();
            services.AddTransient<IBaseRepository<Project>, BaseRepository<Project>>();

            services.AddTransient<IBaseService<BaseEntity>, BaseService<BaseEntity>>();
            services.AddTransient<IBaseService<BaseFile>, BaseService<BaseFile>>();
            services.AddTransient<IBaseService<Campaign>, BaseService<Campaign>>();
            services.AddTransient<IBaseService<Documentation>, BaseService<Documentation>>();
            services.AddTransient<IBaseService<Message>, BaseService<Message>>();
            services.AddTransient<IBaseService<Project>, BaseService<Project>>();

            services.AddTransient<IUserRepository, UserRepository>();

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ICampaignService, CampaignService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IMessageService, MessageService>();
            services.AddTransient<IDocumentationService, DocumentationService>();
            //...
            services.AddTransient<IMapper, Mapper>();
            #endregion


            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
