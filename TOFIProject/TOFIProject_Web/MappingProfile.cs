﻿using AutoMapper;
using System.Collections.Generic;
using TOFIProject_Domain.Entities;
using TOFIProject_Web.Models.Campaigns;
using TOFIProject_Web.Models.Documentations;
using TOFIProject_Web.Models.Messages;
using TOFIProject_Web.Models.Projects;
using TOFIProject_Web.Models.Users;

namespace TOFIProject_Web
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<CampaignModel, Campaign>();
            CreateMap<AppUser, UserModel>();
           // CreateMap<IEnumerable<AppUser>, IEnumerable<UserModel>>();
            CreateMap<Project, ProjectModel>();
            CreateMap<ProjectModel, Project>();
            CreateMap<Message, MessageModel>();
            CreateMap<MessageModel, Message>();
            CreateMap<DocumentationModel, Documentation>();
            CreateMap<Documentation, DocumentationModel>();
            //CreateMap<IEnumerable<Project>, IEnumerable<ProjectModel>>();
        }
    }
}
