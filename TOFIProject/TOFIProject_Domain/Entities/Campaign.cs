﻿using System.Collections.Generic;

namespace TOFIProject_Domain.Entities
{
    public class Campaign: BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<AppUser> Users { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
    }
}
