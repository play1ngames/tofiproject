﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TOFIProject_Domain.Entities
{
    public class Message : BaseEntity
    {
        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }
        public string AppUserId { get; set; }
        public virtual AppUser AppUser { get; set; }

        public string Content { get; set; }
    }
}
