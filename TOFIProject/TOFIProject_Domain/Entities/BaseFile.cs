﻿namespace TOFIProject_Domain.Entities
{
    public class BaseFile: BaseEntity
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Data { get; set; }
    }
}
