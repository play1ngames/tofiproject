﻿using System.Collections.Generic;

namespace TOFIProject_Domain.Entities
{
    public class Project: BaseEntity
    {
        public int CampaignId { get; set; }
        public virtual Campaign Campaign { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }//to do status

        public virtual ICollection<Documentation> Documentation { get; set; }


    }
}
