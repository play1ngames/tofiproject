﻿namespace TOFIProject_Domain.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
