﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TOFIProject_Domain.Entities
{
    public class AppUser : IdentityUser
    {

        public int? CampaignId { get; set; }
        [ForeignKey("CampaignId")]
        public virtual Campaign Campaign { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        //public int AvatarId { get; set; }
        //public virtual Avatar Avatar { get; set; }
    }
}
