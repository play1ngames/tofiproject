﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TOFIProject_Domain.Entities;

namespace TOFIProject_Domain.Contexts
{
    public class Context: IdentityDbContext<AppUser>
    {
        //public DbSet<Avatar> Avatars { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<Documentation> Documentations { get; set; } //todo change_name
        public DbSet<Message> Messages { get; set; }
        public DbSet<Project> Projects { get; set; }

        public Context(DbContextOptions<Context> options) : base(options)
        {

        }
    }
}
