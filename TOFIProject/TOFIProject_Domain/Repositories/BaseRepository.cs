﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TOFIProject_Domain.Contexts;
using TOFIProject_Domain.Entities;

namespace TOFIProject_Domain.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly Context _context;

        private readonly DbSet<T> _entities;

        public BaseRepository(Context context)
        {
            _context = context;
            _entities = context.Set<T>();
        }
        public async Task<IEnumerable<T>> GetAll()
        {
            return await _entities.ToListAsync();
        }
        public async Task<T> GetById(int id)
        {
            return await _entities.SingleOrDefaultAsync(s => s.Id == id);
        }

        public IEnumerable<T> Where(Expression<Func<T, bool>> exp)
        {
            return _entities.Where(exp);
        }

        //Cannot insert blabla when IDENTITY_INSERT is set to off
        //when id is not 0 and is not in Database - fixed
        //when foreignkey is 0 or not in Database - validation on service
        public async void Insert(T entity)
        {
            if (entity == null) throw new ArgumentNullException("Input data is null");
            await _entities.AddAsync(entity);
            _context.SaveChanges();
        }
        public async void Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException("Input data is null");

            var oldEntity = await _context.FindAsync<T>(entity.Id);
            _context.Entry(oldEntity).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }
        public void Delete(T entity)
        {
            if (entity == null) throw new ArgumentNullException("Input data is null");

            _entities.Remove(entity);
            _context.SaveChanges();
        }

    }
}
