﻿using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;
using TOFIProject_Domain.Entities;

namespace TOFIProject_Domain.Repositories
{
    public interface IUserRepository
    {
        IQueryable<AppUser> Get();
        AppUser GetByEmail(string email);
        Task<IdentityResult> Create(AppUser user, string password);
        Task<IdentityResult> Delete(AppUser user);
        Task<IdentityResult> Update(AppUser user);
        UserManager<AppUser> GetUserManager();
    }
}
